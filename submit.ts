import { readFile } from 'node:fs/promises';
import { postSolution } from './requests';
import { puzzleCache } from './caches';
import {
  isPartOneCompleted,
  isPartTwoCompleted,
  getSolutionFns,
} from './utils';

const dayIndex = Bun.argv[2];
console.log(`Day ${dayIndex} https://adventofcode.com/2023/day/${dayIndex}\n`);

if (await isPartTwoCompleted(dayIndex)) {
  console.log('  - This puzzle has already been completed.\n');
  process.exit();
}

if (await isPartOneCompleted(dayIndex)) {
  await submitPartTwo();
} else {
  await submitPartOne();
}

async function submitPartOne() {
  const { solvePartOne } = await getSolutionFns(dayIndex);
  const input = await readFile(`./days/${dayIndex}/input.txt`, 'utf-8');
  const solution = solvePartOne(input);
  console.log(`  - Submitting solution for Part One: ${solution}`);
  const responseText = await postSolution(dayIndex, 1, solution).then((res) =>
    res.text()
  );
  handleResponse(responseText);
}

async function submitPartTwo() {
  const { solvePartTwo } = await getSolutionFns(dayIndex);
  const input = await readFile(`./days/${dayIndex}/input.txt`, 'utf-8');
  const solution = solvePartTwo(input);
  console.log(`  - Submitting solution for Part Two: ${solution}`);
  const responseText = await postSolution(dayIndex, 2, solution).then((res) =>
    res.text()
  );
  handleResponse(responseText);
}

async function handleResponse(responseText: string) {
  const responseThrottledRegex = /You gave an answer.+?left to wait\./;
  const responseIncorrectRegex = /That's not the right.+Please wait .+?\./;

  if (responseThrottledRegex.test(responseText)) {
    const responseThrottlingMsg = (responseText.match(
      responseThrottledRegex
    ) || ['Throttled!'])[0];
    console.error(`  - ${responseThrottlingMsg}\n`);
  } else if (!responseIncorrectRegex.test(responseText)) {
    await puzzleCache.remove(dayIndex);
    console.log(`  - Correct!\n`);
  } else {
    const responseIncorrectMsg = (responseText.match(
      responseIncorrectRegex
    ) || [''])[0];
    console.error(`  - Incorrect. ${responseIncorrectMsg}\n`);
  }
}
