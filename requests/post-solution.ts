import client from '../client';

export async function postSolution(
  dayIndex: string,
  partIndex: number,
  solution: string
) {
  const formData = new URLSearchParams();
  formData.append('level', String(partIndex));
  formData.append('answer', String(solution));
  return client(`/2023/day/${dayIndex}/answer`, formData);
}
