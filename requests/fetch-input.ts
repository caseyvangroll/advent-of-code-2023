import client from '../client';

export async function fetchInput(dayIndex: string) {
  return client(`/2023/day/${dayIndex}/input`).then((res) => res.text());
}
