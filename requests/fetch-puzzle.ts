import client from '../client';
import { puzzleCache } from '../caches';

export async function fetchPuzzle(dayIndex: string) {
  const cachedPuzzle = await puzzleCache.get(dayIndex);
  if (cachedPuzzle) {
    return cachedPuzzle;
  }

  const puzzle = await client(`/2023/day/${dayIndex}`).then((res) =>
    res.text()
  );
  await puzzleCache.set(dayIndex, puzzle);
  return puzzle;
}
