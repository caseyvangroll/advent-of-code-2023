import { mkdir, exists, writeFile } from 'node:fs/promises';
import { fetchInput, fetchPuzzle } from './requests';

export async function isPartOneCompleted(dayIndex: string) {
  const puzzleHtml = await fetchPuzzle(dayIndex);
  return puzzleHtml.includes('--- Part Two ---');
}

export async function isPartTwoCompleted(dayIndex: string) {
  const puzzleHtml = await fetchPuzzle(dayIndex);
  return puzzleHtml.includes('Both parts of this puzzle are complete');
}

export async function initializeDayIfNeeded(dayIndex: string) {
  const initializedDirectory = await exists(`./days/${dayIndex}`);
  if (!initializedDirectory) {
    await mkdir(`./days/${dayIndex}`, { recursive: true });
    console.log('  - Created directory');
  }

  const initializedSolution = await exists(`./days/${dayIndex}/solution.ts`);
  if (!initializedSolution) {
    await writeFile(
      `./days/${dayIndex}/solution.ts`,
      [
        `// Puzzle description: https://adventofcode.com/2023/day/${dayIndex}`,
        '',
        'export function solvePartOne(input: string) {',
        '  // TODO',
        '}',
        '',
        'export function solvePartTwo(input: string) {',
        '  // TODO',
        '}',
      ].join('\n'),
      'utf-8'
    );
    console.log('  - Created template solution file');
  }

  const initializedInput = await exists(`./days/${dayIndex}/input.txt`);
  if (!initializedInput) {
    const input = await fetchInput(dayIndex);
    await writeFile(`./days/${dayIndex}/input.txt`, input, 'utf-8');
    console.log(`  - Retrieved input\n`);
  }
}

export async function getSolutionFns(dayIndex: string) {
  const { solvePartOne, solvePartTwo } = await import(
    `./days/${dayIndex}/solution.ts`
  );
  return { solvePartOne, solvePartTwo };
}
