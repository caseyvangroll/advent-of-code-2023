import { Cache } from 'file-system-cache';

export const puzzleCache = new Cache({ ns: 'puzzles' });
