# advent-of-code-2023

## Installation
```bash
bun install
echo 'AOC_SESSION=""' > .env
```

## Usage
_Note: All of the following scripts require setting the `AOC_SESSION` environment variable._
- Develop solution: `bun run dev <day_number>`
- Submit solution: `bun run submit <day_number>`

### To set the `AOC_SESSION` environment variable
1. Log into https://adventofcode.com
2. Open Chrome devtools (F12) and find/copy the session cookie value
   - `Application Tab -> Storage -> Cookies -> https://adventofcode.com -> session -> Value cell`
3. Place it in `.env` (e.g. `AOC_SESSION="<copied_value>"`)