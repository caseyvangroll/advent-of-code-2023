const { AOC_SESSION } = Bun.env;

// Test that session env variable exists
if (!AOC_SESSION) {
  throw new Error(
    'Missing AOC_SESSION environment variable. Follow README for instructions on how to set.'
  );
}

// Test that session is valid by fetching an input from 2020
const sampleResponse = await client('/2020/day/1/input');
if (!sampleResponse.ok) {
  throw new Error(
    'Stale AOC_SESSION environment variable. Follow README for instructions on how to refresh.'
  );
}

export default async function client(path: string, body?: BodyInit) {
  if (body) {
    return fetch(`https://adventofcode.com${path}`, {
      method: 'POST',
      body,
      headers: {
        Cookie: `session=${AOC_SESSION}`,
        'Content-type': 'application/x-www-form-urlencoded',
      },
      tls: {
        rejectUnauthorized: false,
      },
    });
  }

  return fetch(`https://adventofcode.com${path}`, {
    headers: {
      Cookie: `session=${AOC_SESSION}`,
    },
    tls: {
      rejectUnauthorized: false,
    },
  });
}
