import { describe, expect, test } from 'bun:test';
import { getRankStrength } from './solution';

describe('getRankStrength', () => {
  test.each([
    ['2', 0],
    ['3', 1],
    ['4', 2],
    ['5', 3],
    ['6', 4],
    ['7', 5],
    ['8', 6],
    ['9', 7],
    ['T', 8],
    ['J', 9],
    ['Q', 10],
    ['K', 11],
    ['A', 12],
  ])('getRankStrength(%s) => %d', (rank, expected) => {
    expect(getRankStrength(rank)).toEqual(expected);
  });

  test.each([
    ['J', 0],
    ['2', 1],
    ['3', 2],
    ['4', 3],
    ['5', 4],
    ['6', 5],
    ['7', 6],
    ['8', 7],
    ['9', 8],
    ['T', 9],
    ['Q', 10],
    ['K', 11],
    ['A', 12],
  ])('getRankStrength(%s, true) => %s', (rank, expected) => {
    expect(getRankStrength(rank, true)).toEqual(expected);
  });
});
