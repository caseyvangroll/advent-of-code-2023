// Puzzle description: https://adventofcode.com/2023/day/7

export function solvePartOne(input: string) {
  return solve(input, false);
}

export function solvePartTwo(input: string) {
  return solve(input, true);
}

export function solve(input: string, isPartTwo: boolean) {
  const lines = input.trim().split('\n');
  const hands = lines
    .map((line) => line.split(' '))
    .map(([cards, bid]) => ({
      cards,
      typeStrength: getHandTypeStrength(cards, isPartTwo),
      bid: Number(bid),
    }));
  hands.sort((handA, handB) => {
    if (handA.typeStrength === handB.typeStrength) {
      for (let i = 0; i < 5; i += 1) {
        const charA = handA.cards[i];
        const charB = handB.cards[i];
        if (charA !== charB) {
          return (
            getRankStrength(charA, isPartTwo) -
            getRankStrength(charB, isPartTwo)
          );
        }
      }
    }
    return handA.typeStrength - handB.typeStrength;
  });
  return hands.reduce((acc, hand, i) => acc + hand.bid * (i + 1), 0);
}

export const getHandTypeStrength = (
  cards: string,
  isPartTwo: boolean
): number => {
  const cardCounts = getCardCounts(cards);
  let jokerCt = 0;
  if (isPartTwo) {
    cardCounts.forEach(([rank, count], i) => {
      if (rank === 'J') {
        cardCounts.splice(i, 1);
        jokerCt = count;
      }
    });
  }

  const fiveOfKindCt = cardCounts.filter(([, count]) => count === 5).length;
  const fourOfKindCt = cardCounts.filter(([, count]) => count === 4).length;
  const threeOfKindCt = cardCounts.filter(([, count]) => count === 3).length;
  const pairCt = cardCounts.filter(([, count]) => count === 2).length;

  // Five of a Kind
  if (
    fiveOfKindCt ||
    (fourOfKindCt && jokerCt) ||
    (threeOfKindCt && jokerCt === 2) ||
    (pairCt && jokerCt === 3) ||
    jokerCt >= 4
  ) {
    return 6;
  }

  // Four of a Kind
  if (
    fourOfKindCt ||
    (threeOfKindCt && jokerCt) ||
    (pairCt && jokerCt === 2) ||
    jokerCt === 3
  ) {
    return 5;
  }

  // Full House
  if ((threeOfKindCt && pairCt) || (pairCt === 2 && jokerCt)) {
    return 4;
  }

  // Three of a Kind
  if (threeOfKindCt || (pairCt && jokerCt) || jokerCt === 2) {
    return 3;
  }

  // Two Pair
  if (pairCt === 2 || (pairCt && jokerCt)) {
    return 2;
  }

  // One Pair
  if (pairCt || jokerCt) {
    return 1;
  }

  // High Card
  return 0;
};

export const getCardCounts = (cards: string): Array<[string, number]> => {
  const cardsMap = cards.split('').reduce(
    (acc, rank) => ({
      ...acc,
      [rank]: (acc[rank] || 0) + 1,
    }),
    {} as {
      [rank: string]: number;
    }
  );

  return Object.entries(cardsMap);
};

export const getRankStrength = (
  rank: string,
  isPartTwo: boolean = false
): number => {
  if (isPartTwo) {
    return 'J23456789TQKA'.indexOf(rank);
  }
  return '23456789TJQKA'.indexOf(rank);
};
