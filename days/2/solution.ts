// Puzzle description: https://adventofcode.com/2023/day/2

export function solvePartOne(input: string) {
  const lines = input.trim().split('\n');
  let sumOfIdsOfPossibleGames = 0;
  lines.forEach((line) => {
    const { id, sets } = parseGame(line);
    if (isGamePossible(sets)) {
      sumOfIdsOfPossibleGames += Number(id);
    }
  });
  return sumOfIdsOfPossibleGames;
}

export function solvePartTwo(input: string) {
  const lines = input.trim().split('\n');
  let sumOfPowersOfGames = 0;
  lines.forEach((line) => {
    const { sets } = parseGame(line);
    const { minRedCt, minBlueCt, minGreenCt } = getMinBlockCounts(sets);
    sumOfPowersOfGames += minRedCt * minBlueCt * minGreenCt;
  });
  return sumOfPowersOfGames;
}

type GameType = {
  id: string;
  sets: SetType[];
};

type SetType = {
  redCt: number;
  greenCt: number;
  blueCt: number;
};

export function parseGame(line: string): GameType {
  const [idString, setsString] = line.split(':').map((s) => s.trim());
  const [id] = idString.match(/\d+/) || ['?'];
  return {
    id,
    sets: parseSets(setsString.split(';')),
  };
}

export function parseSets(setStrings: string[]): SetType[] {
  return setStrings.map((setString) => {
    const [, redCtString] = setString.match(/(\d+) red/) || ['', '0'];
    const [, greenCtString] = setString.match(/(\d+) green/) || ['', '0'];
    const [, blueCtString] = setString.match(/(\d+) blue/) || ['', '0'];
    return {
      redCt: Number(redCtString),
      greenCt: Number(greenCtString),
      blueCt: Number(blueCtString),
    };
  });
}

export function isGamePossible(sets: SetType[]): boolean {
  return sets.every(
    (set) => set.redCt <= 12 && set.greenCt <= 13 && set.blueCt <= 14
  );
}

export function getMinBlockCounts(sets: SetType[]) {
  return {
    minRedCt: Math.max(...sets.map((set) => set.redCt)),
    minGreenCt: Math.max(...sets.map((set) => set.greenCt)),
    minBlueCt: Math.max(...sets.map((set) => set.blueCt)),
  };
}
