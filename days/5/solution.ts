// Puzzle description: https://adventofcode.com/2023/day/5

export function solvePartOne(input: string) {
  const sections = input.split('\n\n');
  const seedsToBePlanted = sections[0]
    .split(/\s+/)
    .filter((s) => /\d+/.test(s))
    .map(Number);

  let entries = seedsToBePlanted;
  sections.slice(1).forEach((section) => {
    const deltaRanges = parseDeltaRanges(section);
    entries = entries.map((entry) => {
      for (let i = 0; i < deltaRanges.length; i++) {
        if (deltaRanges[i].contains(entry)) {
          return deltaRanges[i].applyToValue(entry);
        }
      }
      return entry;
    });
  });
  return Math.min(...entries);
}

// --------------------------------------------------------------------------

export function solvePartTwo(input: string) {
  const sections = input.split('\n\n');
  const seedRanges = sections[0]
    .split(/\s+/)
    .filter((s) => /\d+/.test(s))
    .map(Number);
  let ranges: Range[] = [];
  for (let i = 0; i < seedRanges.length; i += 2) {
    ranges.push([seedRanges[i], seedRanges[i] + seedRanges[i + 1] - 1]);
  }

  sections.slice(1).forEach((section) => {
    const deltaRanges = parseDeltaRanges(section);
    const tmpRanges = ranges.flatMap((range) => {
      const result = [];
      let unmappedRanges = [range];
      for (let i = 0; i < deltaRanges.length && unmappedRanges.length; i++) {
        const deltaRange = deltaRanges[i];
        const nextUnmappedRanges: Range[] = [];
        for (let j = 0; j < unmappedRanges.length; j++) {
          if (deltaRange.overlaps(unmappedRanges[j])) {
            result.push(deltaRange.applyToRange(unmappedRanges[j]));
            nextUnmappedRanges.push(
              ...deltaRange.getDifference(unmappedRanges[j])
            );
          } else {
            nextUnmappedRanges.push(unmappedRanges[j]);
          }
        }
        unmappedRanges = nextUnmappedRanges;
      }
      result.push(...unmappedRanges);
      return result;
    });
    ranges = reduceRanges(tmpRanges);
  });
  return ranges[0][0];
}

// --------------------------------------------------------------------------

function parseDeltaRanges(section: string) {
  return section
    .trim()
    .split('\n')
    .slice(1)
    .map((line) => {
      const [destinationRangeStart, sourceRangeStart, rangeLength] =
        line.split(/\s+/).map(Number) || [];
      const startIndex = sourceRangeStart;
      const endIndex = sourceRangeStart + rangeLength - 1;
      const delta = destinationRangeStart - sourceRangeStart;
      return new DeltaRange(startIndex, endIndex, delta);
    });
}

export type Range = [start: number, end: number];

export class DeltaRange {
  startIndex: number;
  endIndex: number; // inclusive
  delta: number;

  constructor(startIndex: number, endIndex: number, delta: number) {
    this.startIndex = startIndex;
    this.endIndex = endIndex;
    this.delta = delta;
  }

  contains(n: number): boolean {
    return n >= this.startIndex && n <= this.endIndex;
  }

  overlaps(range: Range): boolean {
    return !(
      (range[0] < this.startIndex && range[1] < this.startIndex) ||
      (range[0] > this.endIndex && range[1] > this.endIndex)
    );
  }

  getDifference(range: Range): Range[] {
    if (this.overlaps(range)) {
      const difference: Range[] = [];
      if (range[0] < this.startIndex) {
        difference.push([range[0], this.startIndex - 1]);
      }
      if (range[1] > this.endIndex) {
        difference.push([this.endIndex + 1, range[1]]);
      }
      return difference;
    }
    return [range];
  }

  getIntersection(range: Range): Range {
    if (!this.overlaps(range)) {
      throw new Error('Cannot get intersection of non-overlapping range');
    }
    const startIndex = Math.max(range[0], this.startIndex);
    const endIndex = Math.min(range[1], this.endIndex);
    return [startIndex, endIndex];
  }

  applyToValue(n: number): number {
    if (!this.contains(n)) {
      throw new Error('Cannot apply to non-included value');
    }
    return n + this.delta;
  }

  applyToRange(range: Range): Range {
    if (!this.overlaps(range)) {
      throw new Error('Cannot apply to non-overlapping range');
    }
    const [startIndex, endIndex] = this.getIntersection(range);
    return [startIndex + this.delta, endIndex + this.delta];
  }
}

export const reduceRanges = (ranges: Range[]): Range[] => {
  // Sort by ascending start
  ranges.sort(([startA], [startB]) => startA - startB);

  // Reduce overlapping ranges
  const reducedRanges: Range[] = [ranges[0]];
  for (let i = 1; i < ranges.length; i++) {
    const currentRange = ranges[i];
    const lastRange = reducedRanges[reducedRanges.length - 1];
    if (currentRange[0] <= lastRange[1] + 1) {
      lastRange[1] = Math.max(lastRange[1], currentRange[1]);
    } else {
      reducedRanges.push(currentRange);
    }
  }
  return reducedRanges;
};
