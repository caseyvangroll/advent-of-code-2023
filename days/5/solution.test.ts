import { describe, expect, test } from 'bun:test';
import { DeltaRange, Range, reduceRanges } from './solution';

describe('reduceRanges', () => {
  test('should only sort if no overlap', () => {
    const ranges: Range[] = [
      [6, 6],
      [8, 9],
      [1, 4],
    ];
    const expected: Range[] = [
      [1, 4],
      [6, 6],
      [8, 9],
    ];
    expect(reduceRanges(ranges)).toEqual(expected);
  });

  test('should union ranges that are adjacent', () => {
    const ranges: Range[] = [
      [1, 2],
      [2, 3],
    ];
    const expected: Range[] = [[1, 3]];
    expect(reduceRanges(ranges)).toEqual(expected);
  });

  test('should remove ranges that are encapsulated', () => {
    const ranges: Range[] = [
      [1, 3],
      [2, 2],
    ];
    const expected: Range[] = [[1, 3]];
    expect(reduceRanges(ranges)).toEqual(expected);
  });

  test('should union ranges that overlap', () => {
    const ranges: Range[] = [
      [1, 3],
      [2, 4],
    ];
    const expected: Range[] = [[1, 4]];
    expect(reduceRanges(ranges)).toEqual(expected);
  });

  test('should reduce large array of ranges', () => {
    const ranges: Range[] = [
      [9, 19],
      [1, 3],
      [5, 7],
      [74, 78],
      [2, 4],
      [19, 70],
    ];
    const expected: Range[] = [
      [1, 7],
      [9, 70],
      [74, 78],
    ];
    expect(reduceRanges(ranges)).toEqual(expected);
  });
});

describe('DeltaRange', () => {
  describe('overlaps', () => {
    const deltaRange = new DeltaRange(13, 15, -9);
    test('should return true if range is completely encapsulated', () => {
      expect(deltaRange.overlaps([14, 14])).toEqual(true);
    });
    test('should return true if range overlaps from both sides', () => {
      expect(deltaRange.overlaps([10, 19])).toEqual(true);
    });
    test('should return true if range overlaps from left', () => {
      expect(deltaRange.overlaps([10, 13])).toEqual(true);
    });
    test('should return true if range overlaps from right', () => {
      expect(deltaRange.overlaps([15, 19])).toEqual(true);
    });
    test('should return false otherwise', () => {
      expect(deltaRange.overlaps([3, 5])).toEqual(false);
    });
  });

  describe('applyToRange', () => {
    const deltaRange = new DeltaRange(13, 15, -9);
    test('should return range with delta applied if range is completely encapsulated', () => {
      expect(deltaRange.applyToRange([14, 14])).toEqual([5, 5]);
    });
    test('should return range with delta applied if range overlaps from both sides', () => {
      expect(deltaRange.applyToRange([10, 19])).toEqual([4, 6]);
    });
    test('should return subrange with delta applied if range overlaps from left', () => {
      expect(deltaRange.applyToRange([10, 13])).toEqual([4, 4]);
    });
    test('should return subrange with delta applied if range overlaps from right', () => {
      expect(deltaRange.applyToRange([15, 19])).toEqual([6, 6]);
    });
    test('should throw error otherwise', () => {
      expect(() => deltaRange.applyToRange([3, 5])).toThrow();
    });
  });

  describe('getDifference', () => {
    const deltaRange = new DeltaRange(13, 15, -9);
    test('should return empty array if range is completely encapsulated', () => {
      expect(deltaRange.getDifference([14, 14])).toEqual([]);
    });
    test('should return outside ranges if range overlaps from both sides', () => {
      expect(deltaRange.getDifference([10, 19])).toEqual([
        [10, 12],
        [16, 19],
      ]);
    });
    test('should return outside left range if range overlaps from left', () => {
      expect(deltaRange.getDifference([10, 13])).toEqual([[10, 12]]);
    });
    test('should return outside right subrange if range overlaps from right', () => {
      expect(deltaRange.getDifference([15, 19])).toEqual([[16, 19]]);
    });
    test('should return original range if no overlap', () => {
      expect(deltaRange.getDifference([3, 5])).toEqual([[3, 5]]);
    });
  });
});
