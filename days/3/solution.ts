// Puzzle description: https://adventofcode.com/2023/day/3

export function solvePartOne(input: string) {
  const lines = input.trim().split('\n');
  let sumOfPartNumbers = 0;
  for (let lineIndex = 0; lineIndex < lines.length; lineIndex += 1) {
    let possiblePartNumber: string = '';
    let isPartNumber = false;
    for (
      let charIndex = 0;
      charIndex < lines[lineIndex].length;
      charIndex += 1
    ) {
      const curChar = lines[lineIndex][charIndex];
      if (/\d/.test(curChar)) {
        possiblePartNumber = (possiblePartNumber ?? '') + curChar;
        if (getAdjacentSymbols({ lines, lineIndex, charIndex }).length > 0) {
          isPartNumber = isPartNumber || true;
        }
      } else {
        if (possiblePartNumber && isPartNumber) {
          sumOfPartNumbers += Number(possiblePartNumber);
        }
        possiblePartNumber = '';
        isPartNumber = false;
      }
    }
    if (possiblePartNumber && isPartNumber) {
      sumOfPartNumbers += Number(possiblePartNumber);
    }
  }
  return sumOfPartNumbers;
}

// --------------------------------------------------------------------------

export function solvePartTwo(input: string) {
  const lines = input.trim().split('\n');
  const partNumbers = [];
  for (let lineIndex = 0; lineIndex < lines.length; lineIndex += 1) {
    let possiblePartNumber: string = '';
    let adjacentSymbols = [];
    for (
      let charIndex = 0;
      charIndex < lines[lineIndex].length;
      charIndex += 1
    ) {
      const curChar = lines[lineIndex][charIndex];
      if (/\d/.test(curChar)) {
        possiblePartNumber = (possiblePartNumber ?? '') + curChar;
        adjacentSymbols.push(
          ...getAdjacentSymbols({ lines, lineIndex, charIndex })
        );
      } else {
        if (possiblePartNumber && adjacentSymbols.length) {
          partNumbers.push({
            num: Number(possiblePartNumber),
            adjacentSymbols,
          });
        }
        possiblePartNumber = '';
        adjacentSymbols = [];
      }
    }
    if (possiblePartNumber && adjacentSymbols.length) {
      partNumbers.push({
        num: Number(possiblePartNumber),
        adjacentSymbols,
      });
    }
  }

  const gearsMap: {
    [key: string]: number[];
  } = {};
  for (let i = 0; i < partNumbers.length; i += 1) {
    const { num, adjacentSymbols } = partNumbers[i];
    const adjacentGearLocations = new Set<string>();
    adjacentSymbols.forEach(({ char, lineIndex, charIndex }) => {
      if (char === '*') {
        adjacentGearLocations.add(`[${lineIndex}][${charIndex}]`);
      }
    });
    adjacentGearLocations.forEach((location: string) => {
      gearsMap[location] = gearsMap[location] || [];
      gearsMap[location].push(num);
    });
  }

  return Object.values(gearsMap).reduce(
    (sumOfGearRatios, adjacentPartNumbersForGear) => {
      if (adjacentPartNumbersForGear.length === 2) {
        return (
          sumOfGearRatios +
          adjacentPartNumbersForGear[0] * adjacentPartNumbersForGear[1]
        );
      }
      return sumOfGearRatios;
    },
    0
  );
}

const isSymbol = (char: string) => char && !/(\d|\.)/.test(char);
type SchematicSymbol = {
  char: string;
  lineIndex: number;
  charIndex: number;
};
function getAdjacentSymbols({
  lines,
  lineIndex,
  charIndex,
}: {
  lines: string[];
  lineIndex: number;
  charIndex: number;
}): SchematicSymbol[] {
  const adjacentSymbols = [];
  for (let i = lineIndex - 1; i <= lineIndex + 1; i += 1) {
    for (let j = charIndex - 1; j <= charIndex + 1; j += 1) {
      if (isSymbol(lines[i]?.[j])) {
        adjacentSymbols.push({
          char: lines[i]?.[j],
          lineIndex: i,
          charIndex: j,
        });
      }
    }
  }
  return adjacentSymbols;
}
