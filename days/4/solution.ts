// Puzzle description: https://adventofcode.com/2023/day/4

export function solvePartOne(input: string) {
  const lines = input.trim().split('\n');
  let points = 0;
  lines.forEach((line) => {
    const [, winningNumbers, myNumbers] = (
      line.match(/:([^|]+)\|(.+)/) || []
    ).map((s) => s.trim().split(/\s+/g).map(Number));
    const matchingNumberCt = getMatchingNumberCt(winningNumbers, myNumbers);
    if (matchingNumberCt) {
      const pointsForCard = 1 << (matchingNumberCt - 1);
      points += pointsForCard;
    }
  });
  return points;
}

export function solvePartTwo(input: string) {
  const lines = input.trim().split('\n');
  const cardCounts = new Array(lines.length).fill(1);
  lines.forEach((line, cardIndex) => {
    const [, winningNumbers, myNumbers] = (
      line.match(/:([^|]+)\|(.+)/) || []
    ).map((s) => s.trim().split(/\s+/g).map(Number));
    const matchingNumberCt = getMatchingNumberCt(winningNumbers, myNumbers);
    if (matchingNumberCt) {
      for (
        let j = cardIndex + 1;
        j < cardIndex + 1 + matchingNumberCt && j < lines.length;
        j += 1
      ) {
        cardCounts[j] += cardCounts[cardIndex];
      }
    }
  });

  return cardCounts.reduce((cardsCt, cardCt) => cardsCt + cardCt, 0);
}

function getMatchingNumberCt(
  winningNumbers: number[],
  myNumbers: number[]
): number {
  const winningNumbersSet = new Set(winningNumbers);
  return myNumbers.filter((n) => winningNumbersSet.has(n)).length;
}
