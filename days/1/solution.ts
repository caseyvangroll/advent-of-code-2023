// Puzzle description: https://adventofcode.com/2023/day/1

export function solvePartOne(input: string) {
  const lines = input.trim().split('\n');
  let sum = 0;
  lines.forEach((line) => {
    const [, firstDigit] = line.match(/^[^\d]*(\d)/) || [];
    const [, lastDigit] = line.match(/(\d)[^\d]*$/) || [];
    sum += Number(`${firstDigit}${lastDigit}`);
  });
  return sum;
}

export function solvePartTwo(input: string) {
  const lines = input.trim().split('\n');
  let sum = 0;
  lines.forEach((line) => {
    const digits = getDigitsInLine(line);
    const firstDigit = normalizeDigit(digits[0]);
    const lastDigit = normalizeDigit(digits[digits.length - 1]);
    sum += Number(`${firstDigit}${lastDigit}`);
  });
  return sum;
}

function getDigitsInLine(line: string): string[] {
  const r = /(\d|zero|one|two|three|four|five|six|seven|eight|nine)/g;
  const digits = [];
  for (let i = 0; i < line.length; i += 1) {
    r.lastIndex = i;
    const match = r.exec(line);
    if (match) {
      digits.push(match[0]);
    }
  }
  return digits;
}

function normalizeDigit(digit: string): number {
  if (isNaN(Number(digit))) {
    return [
      'zero',
      'one',
      'two',
      'three',
      'four',
      'five',
      'six',
      'seven',
      'eight',
      'nine',
    ].indexOf(digit);
  }
  return Number(digit);
}
