// Puzzle description: https://adventofcode.com/2023/day/6

const sampleInput = `Time:      7  15   30
Distance:  9  40  200`;

function getWinningChargeLengths(race: Race): number[] {
  const winningChargeLengths: number[] = [];
  let inSweetSpot = false;
  for (
    let chargeLengthMs = 1;
    chargeLengthMs < race.durationMs;
    chargeLengthMs += 1
  ) {
    const timeToRun = race.durationMs - chargeLengthMs;
    const distanceCoveredMs = timeToRun * chargeLengthMs;
    if (distanceCoveredMs > race.recordDistanceMm) {
      inSweetSpot = true;
      winningChargeLengths.push(chargeLengthMs);
    } else if (inSweetSpot) {
      return winningChargeLengths;
    }
  }
  return winningChargeLengths;
}

type Race = {
  durationMs: number;
  recordDistanceMm: number;
};

export function solvePartOne(input: string) {
  const lines = input.trim().split('\n');
  const raceDurations = (lines[0].match(/\d+/g) || []).map(Number);
  const raceRecordDistances = (lines[1].match(/\d+/g) || []).map(Number);
  const races = raceDurations.map((durationMs, i) => ({
    durationMs,
    recordDistanceMm: raceRecordDistances[i],
  }));
  let product = 0;
  races.forEach((race) => {
    const winningChargeLengths = getWinningChargeLengths(race);
    product = (product || 1) * winningChargeLengths.length;
  });
  return product;
}

export function solvePartTwo(input: string) {
  const lines = input.trim().split('\n');
  const durationMs = Number(
    (lines[0].match(/\d+/g) || []).reduce((acc, d) => acc + d)
  );
  const recordDistanceMm = Number(
    (lines[1].match(/\d+/g) || []).reduce((acc, d) => acc + d)
  );
  const winningChargeLengths = getWinningChargeLengths({
    durationMs,
    recordDistanceMm,
  });
  return winningChargeLengths.length;
}
