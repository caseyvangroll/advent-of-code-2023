// Puzzle description: https://adventofcode.com/2023/day/9

export function solvePartOne(input: string) {
  const histories = parseInput(input);
  const extrapolatedValues = histories.map((history) => {
    const layers = [history];
    while (!layers[layers.length - 1].every((reading) => reading === 0)) {
      const bottomLayer = layers[layers.length - 1];
      const nextBottomLayer = [];
      for (let i = 0; i < bottomLayer.length - 1; i += 1) {
        nextBottomLayer.push(bottomLayer[i + 1] - bottomLayer[i]);
      }
      layers.push(nextBottomLayer);
    }

    for (let i = layers.length - 1; i > 0; i -= 1) {
      const curLayer = layers[i];
      const curLayerLastVal = curLayer[curLayer.length - 1];
      const nextLayerUp = layers[i - 1];
      const nextLayerUpLastVal = nextLayerUp[nextLayerUp.length - 1];
      nextLayerUp.push(curLayerLastVal + nextLayerUpLastVal);
    }
    return layers[0][layers[0].length - 1];
  });
  return extrapolatedValues.reduce((acc, val) => acc + val, 0);
}

export function solvePartTwo(input: string) {
  const histories = parseInput(input);
  const extrapolatedValues = histories.map((history) => {
    const layers = [history];
    while (!layers[layers.length - 1].every((reading) => reading === 0)) {
      const bottomLayer = layers[layers.length - 1];
      const nextBottomLayer = [];
      for (let i = 0; i < bottomLayer.length - 1; i += 1) {
        nextBottomLayer.push(bottomLayer[i + 1] - bottomLayer[i]);
      }
      layers.push(nextBottomLayer);
    }

    for (let i = layers.length - 1; i > 0; i -= 1) {
      const curLayer = layers[i];
      const curLayerFirstVal = curLayer[0];
      const nextLayerUp = layers[i - 1];
      const nextLayerUpFirstVal = nextLayerUp[0];
      nextLayerUp.unshift(nextLayerUpFirstVal - curLayerFirstVal);
    }
    return layers[0][0];
  });
  return extrapolatedValues.reduce((acc, val) => acc + val, 0);
}

const parseInput = (input: string) =>
  input
    .trim()
    .split('\n')
    .map((line) => line.split(' ').map(Number));
