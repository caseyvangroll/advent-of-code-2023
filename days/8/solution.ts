import lcm from 'compute-lcm';

// Puzzle description: https://adventofcode.com/2023/day/8

export function solvePartOne(input: string) {
  const { instructions, nodesMap } = parseInput(input);
  const mappedInstructions = instructions.map((char) => (char === 'L' ? 0 : 1));

  let stepCt = 0;
  let instructionIndex = 0;
  let curNodeId = 'AAA';
  const targetNodeId = 'ZZZ';
  while (curNodeId !== targetNodeId) {
    stepCt += 1;
    const i = mappedInstructions[instructionIndex];
    curNodeId = nodesMap[curNodeId][i];
    instructionIndex = (instructionIndex + 1) % instructions.length;
  }

  return stepCt;
}

export function solvePartTwo(input: string) {
  const { instructions, nodesMap } = parseInput(input);
  const mappedInstructions = instructions.map((char) => (char === 'L' ? 0 : 1));

  let curNodes = Object.keys(nodesMap).filter((nodeId) => nodeId.endsWith('A'));
  const cycleLengths: number[] = new Array(curNodes.length).fill(0);

  let stepCt = 0;
  let instructionIndex = 0;
  while (cycleLengths.some((cycleLength) => cycleLength === 0)) {
    stepCt += 1;
    const i = mappedInstructions[instructionIndex];
    curNodes = curNodes.map((nodeId, nodeIndex) => {
      const nextNodeId = nodesMap[nodeId][i];
      if (nextNodeId.endsWith('Z') && cycleLengths[nodeIndex] === 0) {
        cycleLengths[nodeIndex] = stepCt;
      }
      return nextNodeId;
    });
    instructionIndex = (instructionIndex + 1) % instructions.length;
  }

  return lcm(cycleLengths);
}

type NodesMap = {
  [nodeId: string]: [leftNodeId: string, rightNodeId: string];
};

function parseInput(input: string) {
  const sections = input.trim().split('\n\n');
  const instructions = sections[0].trim().split('');
  const nodesMap: NodesMap = sections[1]
    .trim()
    .split('\n')
    .reduce((acc, line) => {
      const [nodeId, leftNodeId, rightNodeId] = line.match(/([\dA-Z]+)/g) || [
        '',
      ];
      return {
        ...acc,
        [nodeId]: [leftNodeId, rightNodeId],
      };
    }, {});

  return {
    instructions,
    nodesMap,
  };
}
