import { readFile } from 'node:fs/promises';
import {
  isPartOneCompleted,
  initializeDayIfNeeded,
  getSolutionFns,
} from './utils';

const dayIndex = Bun.argv[2];
console.log(`Day ${dayIndex} https://adventofcode.com/2023/day/${dayIndex}\n`);

if (await isPartOneCompleted(dayIndex)) {
  await runPartOneSolution();
  await runPartTwoSolution();
} else {
  await initializeDayIfNeeded(dayIndex);
  await runPartOneSolution();
}

async function runPartOneSolution() {
  const { solvePartOne } = await getSolutionFns(dayIndex);
  const input = await readFile(`./days/${dayIndex}/input.txt`, 'utf-8');
  try {
    const solution = solvePartOne(input);
    console.log(`  - Part One Solution: ${solution}`);
  } catch (e) {
    console.error('  - Uncaught error while solving Part One.');
    console.error(e);
  }
}

async function runPartTwoSolution() {
  const { solvePartTwo } = await getSolutionFns(dayIndex);
  const input = await readFile(`./days/${dayIndex}/input.txt`, 'utf-8');
  try {
    const solution = solvePartTwo(input);
    console.log(`  - Part Two Solution: ${solution}`);
  } catch (e) {
    console.error('  - Uncaught error while solving Part Two.');
    console.error(e);
  }
}
